import java.util.ArrayList;
import java.util.List;

public class ListInJava {

	public static void main(String[] args) {

		List<String> lst = new ArrayList<>();
		List<String> lst1 = new ArrayList<>();
		List<String> lst2 = new ArrayList<>();

		lst.add("rounak");
		lst.add("Jain");
		lst.add("shubham");
		lst.add(1, "mansi");

		lst1.add("rounak1");
		lst1.add("Jain1");
		lst1.add("shubham1");
		lst1.add(1, "mansi1");

		lst2.add("rounak2");
		lst2.add("Jain2");
		lst2.add("shubham2");
		lst2.add(1, "mansi2");

		List<List<String>> lst3 = new ArrayList<>();
		lst3.add(lst1);
		lst3.add(lst2);
		lst3.add(lst);
//		System.out.println(lst3);

		for (int i = 0; i < lst3.size(); i++) {

			for (int j = 0; j < lst3.get(0).size(); j++) {
				System.out.println(lst3.get(0).get(j));
			}
		}

	}
}
