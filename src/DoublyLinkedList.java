
public class DoublyLinkedList {

	int size;

	class Node {
		int data;
		Node previous;
		Node next;

		public Node(int item) {
			this.data = item;
		}
	}

	Node head = null;
	Node tail = null;

	public void addFirst(int data) {
		Node newNode = new Node(data);
		newNode.next = head;
		newNode.previous = null;
		if (head != null) {
			head.previous = newNode;
		}
		head = newNode;
		if (tail == null) {
			tail = newNode;
		}
		size++;
	}

	public void addLast(int data) {
		Node newNode = new Node(data);
		newNode.data = data;
		newNode.next = null;
		newNode.previous = tail;
		if (tail != null) {
			tail.next = newNode;
		}

		tail = newNode;
		if (head == null) {
			head = newNode;
		}
		size++;
	}

	public void printList() {
		Node current = head;
		while (current != null) {
			System.out.print(current.data + "-->");
			current = current.next;
		}
		System.out.println();
	}

	public void printListBackward() {
		Node current = tail;
		while (current != null) {
			System.out.print(current.data + "-->");
			current = current.previous;
		}
		System.out.println();
	}

	public void addAtIndex(int data, int index) {
		Node newNode = new Node(data);
		newNode.next = null;
		newNode.previous = null;

		if (index < 1) {
			System.out.println(" Linked List should be greater then 1");
		} else if (index == 1) {
			newNode.next = head;
			head.previous = newNode;
			head = newNode;
		}else {
			Node temp= new Node(data);
			temp=head;
			
			
			for(int i=1;i<index-1;i++) {
				if(temp!=null) {
					temp=temp.next;
				}
			}	
			if(temp!=null) {
			
				newNode.next=temp.next;
				newNode.previous=temp;
				temp.next=newNode;
				if(newNode.next!=null) {
					newNode.next.previous=newNode;
				}
			}
			
		}
		

	}

	public static void main(String[] args) {

		DoublyLinkedList list = new DoublyLinkedList();
		list.addFirst(10);
		list.addFirst(23);
		list.addFirst(34);
//		list.printList();
		list.addLast(56);
list.addAtIndex(3, 2);
		list.printList();
		list.printListBackward();
	}

}
