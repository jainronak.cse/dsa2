package StringInJava;

public class StringReverseUsingRecursion {

	
	public static void strReverse(String str, int len) {
		if(len==0) {
			System.out.println(str.charAt(len));
			return;
		}
		
		System.out.print(str.charAt(len));
		strReverse(str,len-1);
	}
	
	public static void main(String[] args) {
		String str = "tutorial";
		strReverse(str,str.length()-1);
	}
	
}
