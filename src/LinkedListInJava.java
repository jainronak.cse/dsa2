import java.util.LinkedList;
import java.util.StringTokenizer;

public class LinkedListInJava {
	Node head;
	private int size;

	LinkedList e;

	LinkedListInJava() {
		size = 0;
	}

	public void add(int index, String data) {
		if (index > size || index < 0) {
			return;
		}
		size++;
		Node newNode = new Node(data);
		if (head == null || index == 0) {
			newNode.next = head;
			head = newNode;
			return;
		}

		Node currentNode = head;
		for (int i = 1; i < size; i++) {
			if (i == index) {
				Node nextNode = currentNode.next;
				currentNode.next = newNode;
				newNode.next = nextNode;
				break;
			}
			currentNode = currentNode.next;
		}
	}

	public class Node {
		String data;
		Node next;

		Node(String data) {
			this.data = data;
			this.next = null;
			size++;
		}
	}

	public void reverseIterate() {
		if(head==null || head.next==null) {
			System.out.println("Either list is empty or only one element");
		}

		
		Object o;
		
		
		Node prevNode=head;
		Node currentNode= head.next;
		while(currentNode!=null) {
			Node nextNode = currentNode.next;
			currentNode.next=prevNode;
			
			prevNode=currentNode;
			currentNode=nextNode;
		}
		head.next=null;
		head=prevNode;
	}
	
	
	public void addFirst(String data) {
		Node newNode = new Node(data);
		newNode.next = head;
		head = newNode;
	}

	public void addLast(String data) {
		Node newNode = new Node(data);
		if (head == null) {
			head = newNode;
			return;
		}
		Node lastNode = head;
		while (lastNode.next != null) {
			lastNode = lastNode.next;
		}
		lastNode.next = newNode;
	}

	public int size() {
		return size;
	}

	public void printList() {
		Node currNode = head;
		while (currNode != null) {
			System.out.print(currNode.data + "-->");
			currNode = currNode.next;
		}
		System.out.println("Null");
	}

	public void removeFirst() {
		if (head == null) {
			System.out.println("Empty linked list");
			return;
		}
		head = this.head.next;
		size--;
	}

	public void removeLast() {
		if (head == null) {
			System.out.println("Empty linked list");
			return;
		}
		size--;
		if (head.next == null) {
			head = null;
			return;
		}

		Node currNode = head;
		Node lastNode = head.next;
		while (lastNode.next != null) {
			currNode = currNode.next;
			lastNode = lastNode.next;
		}

		currNode.next = null;

	}

	public static void main(String[] args) {
		LinkedListInJava obj = new LinkedListInJava();
		obj.addFirst("jain");
		obj.addFirst("rounak");
		obj.addFirst("am");
		obj.addFirst("I");
//		obj.addLast("askdfjas");
		obj.printList();
		obj.reverseIterate();
		obj.printList();

//		obj.add(2, "New Ele");
//		obj.printList();
//		System.out.println(obj.size());

	}

}
