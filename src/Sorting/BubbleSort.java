package Sorting;

public class BubbleSort {

	public static void main(String[] args) {
		
		int arr[] = {4,3,2,5,6,0};
		int n = arr.length;
		int temp;
		boolean swap;
		for(int i=0;i<n-1;i++) {
			swap=true;
			for(int j=0;j<n-1-i;j++) {
				
				if(arr[j]>arr[j+1]) {
					temp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=temp;
					swap=false;
				}			
			}
			if(swap==true) {
				break;
			}
		}
		
		for(int i=0;i<arr.length;i++) {
			System.out.println(arr[i]);
		}
		
	}
}
