package Backtracking;

import java.util.ArrayList;
import java.util.List;

public class NqueenProblem {

	
	public void saveTheBoard(char[][] board, List<List<String>> wholeBoard) {
		String row="";
		List<String> newBoard = new ArrayList<>();
		for(int i=0;i<board.length;i++) {
			row="";
			for(int j=0;j<board[0].length;j++) {
				if(board[i][j]=='Q') {
					row=row+'Q';
				}else {
					row=row+'.';
				}
			}
			newBoard.add(row);
		}
		wholeBoard.add(newBoard);
	}
	
	public void helper(char[][] board, List<List<String>> wholeBoard, int col) {
		if (col == board.length) {
			saveTheBoard(board, wholeBoard);
			return;
		}

		for (int r = 0; r < board.length; r++) {
			if (isSafe(r, col, board)) {
				board[r][col] = 'Q';
				helper(board, wholeBoard, col + 1);
				board[r][col] = '.';
			}
		}

	}

	public boolean isSafe(int row, int col, char[][] board) {
		// Horizontal
		for (int i = 0; i < board.length; i++) {
			if (board[row][i] == 'Q') {
				return false;
			}
		}
		// vertical
		for (int i = 0; i < board.length; i++) {
			if (board[i][col] == 'Q') {
				return false;
			}
		}

		// Upper Left
		int r = row;
		for (int i = col; i >= 0 && r >= 0; i--, r--) {
			if (board[r][i] == 'Q') {
				return false;
			}
		}

		// Upper right
		r = row;
		for (int i = col; i < board.length && r >= 0; i++, r--) {
			if (board[r][i] == 'Q') {
				return false;
			}
		}

		// Lower left
		r = row;
		for (int i = col; i >= 0 && r < board.length; i--, r++) {
			if (board[r][i] == 'Q') {
				return false;
			}
		}

		// Lower right
		r = row;
		for (int i = col; i < board.length && r < board.length; i++, r++) {
			if (board[r][i] == 'Q') {
				return false;
			}
		}
		return true;
	}

	public List<List<String>> solveNQueen(int n) {
		List<List<String>> wholeBoard = new ArrayList<>();
		char[][] board = new char[n][n];
		helper(board, wholeBoard, 0);
		return wholeBoard;
	}

	public static void main(String[] args) {

		int n = 4;
		NqueenProblem obj = new NqueenProblem();
		List<List<String>> wholeBoard = obj.solveNQueen(n);
		System.out.println(wholeBoard.get(0));
		System.out.println(wholeBoard.get(1));

	}
}
