package RecursionInJAva;

public class Program {

	
	public static int printPower(int x,int n) {
		if(n==1) {
			return x;
		}
		if(x==0) {
			return 0;
		}
		
		
		return x*printPower(x,n-1);
	}
	
	public static void main(String[] args) {
		System.out.println(printPower(2,4));
	}
	
	
}
