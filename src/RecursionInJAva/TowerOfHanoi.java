package RecursionInJAva;

public class TowerOfHanoi {

	public static void toi(int n, String src, String helper, String dest) {
		if (n == 0) {
//			System.out.println("Transfer disk " + n + " From " + src + " to " + dest);
			return;
		}
		toi(n - 1, src, dest, helper);
		System.out.println("Transfer disk " + n + " From " + src + " to " + dest);
		toi(n - 1, helper, src, dest);
	}
	
	public static void main(String[] args) {
		int n=3;
		toi(n,"A","B","C");
	}
}
