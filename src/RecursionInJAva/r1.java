package RecursionInJAva;

public class r1 {

	public void show(int i) {
		System.out.println(i);
		if (i == 5) {
			return;
		}
		i++;
		show(i);
		System.out.println(i);
	}

	public static void main(String[] args) {
		r1 obj = new r1();
		obj.show(1);
	}
}
