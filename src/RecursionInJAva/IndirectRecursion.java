package RecursionInJAva;

public class IndirectRecursion {

	int n=10;
	int i=1;
	
	
	public void fun1() {
		if(i<=n) {
			System.out.println(i);
			i++;
			fun2();
		}else {
			return;
		}
	}
	public void fun2() {
		if(i<=n) {
			System.out.println(i);
			i++;
			fun1();
		}else {
			return;
		}
	}
	
	
	
	public static void main(String[] args) {
		
		IndirectRecursion obj = new IndirectRecursion();
		obj.fun1();
	}
	
}
