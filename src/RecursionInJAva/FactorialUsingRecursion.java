package RecursionInJAva;

public class FactorialUsingRecursion {

	public int factorial(int i) {
		if(i==0) {
			return 1;
		}
		return i*factorial(i-1);
	}
	public int naturalNumberSum(int i) {
		if(i==1) {
			return 1;
		}
		return i+naturalNumberSum(i-1);
	}
	
	
	public static void main(String[] args) {
		
		FactorialUsingRecursion obj = new FactorialUsingRecursion();
//		int i =obj.factorial(5);
//		System.out.println(i);
		System.out.println(obj.naturalNumberSum(5));
	}
}
