package RecursionInJAva;

public class TailRecursion {

	public int factCal(int n,int a) {
		if(n<=0) {
			return a;
		}
		return factCal(n-1,n*a);
	}
	public int fact(int n) {
		return factCal(n,1);
	}
	
	public static void main(String[] args) {
		TailRecursion obj  = new TailRecursion();
		System.out.println(obj.fact(5));
	}
}
