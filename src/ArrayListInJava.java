import java.util.ArrayList;

public class ArrayListInJava {

	public static void swap(int i, int j, ArrayList<Integer> al1) {
		int temp = al1.get(i);
		al1.set(i, al1.get(j));
		al1.set(j, temp);
		System.out.println(al1);

	}

	public static void revList(ArrayList<Integer> al) {
		for (int i = al.size() - 1; i >= 0; i--) {
			System.out.println(al.get(i));
		}
	}

	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<>();
		al.add(1);
		al.add(2);
		al.add(3);
		al.add(5);
//		ArrayListInJava.revList(al);
		ArrayListInJava.swap(0, 2, al);
	}
}
