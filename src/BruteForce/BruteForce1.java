package BruteForce;

import java.util.ArrayList;

public class BruteForce1 {

	public static int storeWater(ArrayList<Integer> al1) {
		int maxWater = 0;
		int arr[] = new int[2];
		for (int i = 0; i < al1.size(); i++) {
			for (int j = i + 1; j < al1.size(); j++) {
				int height = Math.min(al1.get(i), al1.get(j));
				int width = j - i;
				int currentArea = height * width;
				int t= maxWater;
				maxWater = Math.max(maxWater, currentArea);
				if(maxWater>t) {
					arr[0]=i;
					arr[1]=j;
				}
			}
		}

		System.out.println(arr[0]);
		System.out.println(arr[1]);

		return maxWater;
	}

	public static void main(String[] args) {
		ArrayList<Integer> al = new ArrayList<>();
		al.add(1);
		al.add(0);
		al.add(6);
		al.add(2);
		al.add(5);
		al.add(4);
		al.add(8);
		al.add(3);
		al.add(7);
		System.out.println(al);
		System.out.println(BruteForce1.storeWater(al));
	}
}
