package FunctionInJava;

public class FunctionInj {

	public void show() {
		System.out.println("in show");
	}
	public void evenOrOdd(int i) {
		if(i%2==0) {
			System.out.println("Even Number");
		}else {
			System.out.println("Odd number");
		}
	}
	public void printBonus(boolean n) {
		if(n==true) {
			System.out.println("bonus is 100");
		}else {
			System.out.println("bonus is 200");
		}
	}
	
	public boolean evenOrOdd1(int i) {
		boolean b1=false;
		if(i%2==0) {
//			System.out.println("Even Number");
			b1=true;
		}else {
//			System.out.println("Odd number");
			b1= false;
		}
		return b1;
	}
	
	
	public static void main(String[] args) {
		FunctionInj obj = new FunctionInj();
//		obj.show();
//		obj.evenOrOdd(23);
		boolean b =obj.evenOrOdd1(24);
//		System.out.println(b);
		obj.printBonus(b);
		
	}
}
