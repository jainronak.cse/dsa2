package StackInJAva;

public class NestedClassInJava {

	class inner{
		void msg() {
			System.out.println("In msg");
		}
	}
	
	public static void main(String[] args) {
		NestedClassInJava obj = new NestedClassInJava();
		NestedClassInJava.inner obj1 = obj.new inner();
		obj1.msg();
		
	}
	
}
