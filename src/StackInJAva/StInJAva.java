package StackInJAva;

import java.util.ArrayList;
import java.util.Stack;

public class StInJAva {

	static class Stack<T> {
		ArrayList<T> list = new ArrayList<>();

		public void push(T data) {
			list.add(data);
		}

		public boolean isEmpty() {
			return list.size() == 0;
		}

		public T pop() {
			if (isEmpty()) {
				return null;
			}
			T top = list.remove(list.size() - 1);
			return top;
		}

		public T peek() {
			if (isEmpty()) {
				return null;
			}
			return list.get(list.size() - 1);
		}

	}

	public static void main(String[] args) {

		Stack stack = new Stack();
		stack.push("rounak");
		stack.push(2);
		stack.push(3);
		stack.push(4);
		
		while(!stack.isEmpty()) {
		
			System.out.println(stack.pop());
			
		}

		
		

	}
}
