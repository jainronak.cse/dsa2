package StackInJAva;

public class LocalInnerClass {

	int data = 30;
	void display() {
		class Local{
			void eat() {
				System.out.println("in eat");
			}
		}
		Local l = new Local();
		l.eat();
	}
	
	public static void main(String[] args) {
		
		LocalInnerClass obj = new LocalInnerClass();
		obj.display();
	}
	
}

